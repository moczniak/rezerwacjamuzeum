(function($) {



function loadPanel(load)
{
	
	
	$.ajax({
		url : 'index.php',
			data: 'action=load&tpl='+load,
			type: 'POST',
			success: function(data) {
				$('#AdminPanelModal').html(data);
			}
	});
	
}

/* ADMIN */

if(window.location.hash) {
      var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
	
	if(hash == 'admin')
	{
		setTimeout(function(){
			
			$.ajax({
				url : 'index.php',
					data: 'action=checkLogin',
					type: 'POST',
					success: function(data) {
						if(data == 'ok')
						{
							loadPanel('adminPanel');
						}
						$('#adminModal').modal('show');
					}
			});		  
		}, 200);	
	}	
}

$('.loginbtn').click(function(e){
	e.preventDefault();
		var login = $('.loginInput').val();
		var psw = $('.pswInput').val();
		if(login != '' && psw != '')
		{
			$.ajax({
				url : 'index.php',
				data: 'action=login&login='+login+'&psw='+psw,
				type: 'POST',
				success: function(data) {
						if(data != 'ok')
						{
							$('.formError').html(data);
						}else
						{
							loadPanel('adminPanel');
						}
				}
			});
		}
		else
		{
				$('.formError').html("Nie zostawiaj pustych pól");
		}
	
	
});







/* ADMIN */



}(jQuery));