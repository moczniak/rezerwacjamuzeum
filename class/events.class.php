<?php


/*
{
	"success": 1,
	"result": [
		{
			"id": "293",
			"title": "SiemaSiema",
			"url": "#",
			"class": "event-warning",
			"start": "1362938400000",
			"end":   "136303800000"
		}
	]
}

*/

Class Event
{
	
	
	
	public function past_events()
	{
		$json = new Json();
		$user = new User();
		$data = json_decode($json->open('events.json'),true);
		$out = array();
		if(!empty($data['result'])){
			foreach($data['result'] as $d)
			{
				if(($d['end'] / 1000) > time()){
					$out[] = array(
						'id' => $d['id'],
						'title' => $d['title'],
						'url' => "#",
						'clients'=>$d['clients'],
						'class' => $d['class'],
						'start' => $d['start'],
						"end"=>$d['end']
					);
				}
				else
				{
					$past++;
					//wyczyscic uczestnikow!:>
					$user->clearUserFromEvent($d['id']);
				}
			}
		}
		if($past > 0)
		{
			$data = json_encode(array('success' => 1, 'result' => $out));
			$json2 = new Json();
			$json2->setFile('events.json');
			$json2->saveNew($data);
			$dane = $this->prepareEvents();
			if(!empty($dane)){
				$json2->saveNew($dane);
			}
		}
	}
	
	public function prepareEvents()
	{
		$json = new Json();
		$data = json_decode($json->open('events.json'),true);
		$settings = json_decode($json->open('settings.json'),true);
		/*
		foreach($data['result'] as $d)
		{
			print_r(date('l',$d['start']/1000));
		}
		*/
		$many = count($data['result']);
		$many = _DNI_DO_PRZODU - count($data['result']);
		if(empty($data))
		{
			//ile liczyc?
			//30 dni do przodu z zachowaniem IDSOW,
			//today!
			$today = strtotime(date('Y-m-d'));
			$id = $data['result'][count($data['result'])]['id']+1;
			$licz = $id;
			$out = array();
			for($i = 0; $i< $many;++$i)
			{
				$todayD = date('N',$today) - 1;
				if(!empty($settings[$todayD])){
					//echo $todayD.'<br>';
					foreach($settings[$todayD] as $td){
						$out[] = array(
							'id' => $id,
							'title' => "test",
							'url' => "#",
							'clients'=>0,
							'class' => 'event-important',
							'start' => ($today+$td['time1']).'000',
							"end"=>($today+$td['time2']).'000'
						);
						$id++;
					}
				}
				$today = $today + (24 * 60 * 60);
			}
			$data = json_encode(array('success' => 1, 'result' => $out));
			return $data;

		}else
		{
			if($many > 0)
			{
				$last = end($data['result']);
				$id = $last['id']+1;
				$licznik = count($data['result']);
			$out = $data['result'];
			$today = strtotime(date('Y-m-d',$last['start']/1000)) + (24 * 60 * 60);;
			
			for($i = 0; $i< $many;++$i)
			{
				$todayD = date('N',$today) - 1;
				if(!empty($settings[$todayD])){
					foreach($settings[$todayD] as $td){
						$today = strtotime(date('Y-m-d',$today).' 00:00:00');
						
							$out[] = array(
								'id' => $id,
								'title' => "test",
								'url' => "#",
								'clients'=>0,
								'class' => 'event-important',
								'start' => ($today+$td['time1']).'000',
								"end"=>($today+$td['time2']).'000'
							);
							$id++;
					}
				}
				$today = $today + (24 * 60 * 60);
			}
			$data = json_encode(array('success' => 1, 'result' => $out));
			return $data;
				
				
			}
		}
		
		
	}
	
}