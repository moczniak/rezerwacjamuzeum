<!DOCTYPE html>
<html>
<head>
	<title>Muzeum Śląskie Rezerwacja Wycieczek</title>

	<meta charset="UTF-8">

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="assets/css/calendar.css">
	<link rel="stylesheet" href="assets/css/bootstrap-slider.css">

</head>
<body>
<div class="container">


	<div class="page-header">

		<div class="pull-right form-inline">
			<div class="btn-group">
				<button class="btn btn-primary" data-calendar-nav="prev"><< Poprzedni</button>
				<button class="btn btn-default" data-calendar-nav="today">Dzisiaj</button>
				<button class="btn btn-primary" data-calendar-nav="next">Następny >></button>
			</div>
			<div class="btn-group">
				<button class="btn btn-warning" data-calendar-view="year">Rok</button>
				<button class="btn btn-warning active" data-calendar-view="month">Miesiąc</button>
				<button class="btn btn-warning" data-calendar-view="week">Tydzień</button>
				<button class="btn btn-warning" data-calendar-view="day">Dzień</button>
			</div>
		</div>

		<h3></h3>
		<span style="color:green; font-weight:bold;font-size:18px;"><?=$success;?></span>
		<span style="color:red; font-weight:bold;font-size:18px;"><?=$error;?></span>
		<small><br>Aby zobaczyc wydarzenia w danym dniu nalezy dwa razy kliknąc w dany dzień</small>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div id="calendar"></div>
		</div>
		<!--
		<div class="col-md-3">


			<h4>Events</h4>
			<small>This list is populated with events dynamically</small>
			<ul id="eventlist" class="nav nav-list"></ul>

		</div>
		--->
	</div>

	<div class="clearfix"></div>
	<br><br>



	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/underscore-min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jstz.min.js"></script>
	<script type="text/javascript" src="assets/js/language/pl-PL.js"></script>
	<script type="text/javascript" src="assets/js/calendar.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap-slider.js"></script>
	

</div>


<div class="modal fade" id="events-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Event</h3>
            </div>
            <div class="modal-body" style="height: 400px">
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn">Close</a>
            </div>
        </div>
    </div>
</div>




<!-- Modal ADMIN-->
<div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">AdminPanel</h4>
      </div>
      <div class="modal-body" id="AdminPanelModal">
        <p>Zaloguj się</p>
		<p style="color:red;" class="formError"></p>
		
		<form class="form-inline" role="form">
  <div class="form-group">
    <label class="sr-only" for="loginForm">Login</label>
    <input type="text" class="form-control loginInput" id="loginForm" placeholder="Login">
  </div>
  <div class="form-group">
    <label class="sr-only" for="hasloForm">Hasło</label>
    <input type="password" class="form-control pswInput" id="hasloForm" placeholder="Hasło">
  </div>
  <button type="submit" class="btn btn-default loginbtn">Zaloguj</button>
</form>
		
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="app.js"></script>
<script type="text/javascript" src="admin.js"></script>

</body>
</html>
